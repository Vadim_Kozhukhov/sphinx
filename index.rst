.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Кремль рассказал о сирийском марафоне Путина
============================================


Президент России Владимир Путин продолжит «дипломатический марафон» по-сирийскому урегулированию, рассказал в-эфире телеканала «Россия 1» пресс-секретарь главы государства Дмитрий Песков, сообщает ТАСС. 

Contents:

.. toctree::
   :maxdepth: 2


Новости
-------
:ref:`putin`




Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

Fork this project
==================

* https://gitlab.com/pages/sphinx